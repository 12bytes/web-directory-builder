#!/usr/bin/env bash
set -h -u -o 'pipefail'

#shellcheck disable=SC1003,SC2312
# SC1003=Want to escape a single quote?
# SC2312 Consider invoking this command separately

# SCRIPT INFORMATION
# ------------------------------------------------------------------------------
# Name          : Web Directory Builder (wbd.sh)
# License       : Non-Profit Open Software License ("Non-Profit OSL") 3.0
# Dependencies  : mapfile, tidy
# Author        : 12bytes.org
# Code          : https://codeberg.org/12bytes/aid
#
# Comment       : See the included README.md file for help.
#
# LICENSE
# ------------------------------------------------------------------------------
# This program is free software as afforded by the Non-Profit Open Software
# License ("Non-Profit OSL") 3.0, a copy of which should have been included. The
# license can be viewed on-line at: https://opensource.org/licenses/NPOSL-3.0
# ------------------------------------------------------------------------------
#

# TODO
#
# we could grab website favicons
# curl --output="${sDomain}.WHAT?" "https://t0.gstatic.com/faviconV2?client=SOCIAL&type=FAVICON&url=${sUrl}&size=16"
# curl --output="${sDomain}.WHAT?" "https://t1.gstatic.com/faviconV2?client=SOCIAL&type=FAVICON&fallback_opts=TYPE,SIZE,URL&url=${sUrl}&size=16"
# "t<n>" can be 0 to 3
# size seems to default to 16x16 if not specified
# another one: https://api.faviconkit.com/&${sDomain}/32

sScriptVersion='20250124'
sScriptRepoURL='https://codeberg.org/12bytes/web-directory-builder'
sDirectory=''
sCat=''
sItem=''
sTitle=''
aCatFiles=()
# char encoding/replacements for titles, descriptions
#declare -A aaPrintChars=(['&']='&amp;' ['<']='&lt;' ['>']='&gt;' ['“']='"' ['”']='"' ['„']='"' ['❛']="'" ['❜']="'" ["’"]="'" ['‛']="'" ['`']="'" ['´']="'")
# if using iconv (not all these are necessary if using tidy)
declare -A aaPrintChars=(['&']='&amp;' ['<']='&lt;' ['>']='&gt;' ['„']='"' ['❛']="'" ['❜']="'"  ['`']="'")
# encode some unsafe chars for URLs (not all these are necessary if using tidy)
declare -A aaUrlChars=(['"']='%22' ['<']='%3C' ['>']='%3E' ['{']='%7B' ['}']='%7D' ['|']='%7C' ['\']='%5C' ['^']='%5E' ['`']='%60' [' ']='%20')

# BEGIN INITIALIZE

a=(
    ''
    '  -----------------------------------'
    "    WEB DIRECTORY BUILDER v${sScriptVersion}"
    '                     by 12bytes.org'
    '  -----------------------------------'
    ''
    'See the README.md file for help.'
)
printf '%s\n' "${a[@]}"

# END INITIALIZE

while true ; do
    if [[ -n "${sDirectory}" ]] ; then
        a=(
            ''
            "Directory  : ${sDirectory}"
        )
        if [[ -n "${sCat}" ]] ; then
            a+=("Category   : ${sCat}")
            [[ -n "${sItem}" ]] && a+=("Item title : ${sTitle}")
        fi
        printf '%s\n' "${a[@]}"
    fi
    printf '\n%s\n' 'Select an option...'
    if [[ -z "${sDirectory}" ]] ; then
        a=('Add a directory' 'Select directory')
    elif [[ -z "${sCat}" ]] ; then
        a=('Add a category' 'Select category' 'Rebuild directory')
    elif [[ -z "${sItem}" ]] ; then
        a=('Add primary website')
    else
        a=('Add secondary website' 'Publish item')
    fi
    select sSelect in "${a[@]}" 'Quit' ; do
        case "${sSelect}" in
            ('Add a directory')
                read -erp 'Directory title: '
                [[ -z "${REPLY}" ]] && continue 2
                if [[ -d "directories/${REPLY}" ]] ; then
                    printf '%s\n' 'ERROR: Directory already exists.' && sleep 2 && continue 2
                fi
                sDirectory="${REPLY}"

                printf '%s\n' 'Creating directories...'
                mkdir --parents -- "backups" "directories/${sDirectory}/assets" "directories/${sDirectory}/cats" "directories/${sDirectory}/templates" "directories/${sDirectory}/output/resources" || exit 1

                printf '%s\n' 'Copying files...'
                cp --archive -- 'master-templates/.'  "directories/${sDirectory}/templates/" || exit 1

                a=(
                    ''
                    'Finished!'
                    "Directory created: directories/${sDirectory}"
                    "Edit the template files in the directories/${sDirectory}/templates folder and restart the script."
                )
                printf '%s\n' "${a[@]}" && exit
            ;;

            ('Select directory')
                # get names for all the directory folders
                mapfile -t a < <(find 'directories/' -mindepth 1 -maxdepth 1 -type 'd' -exec basename {} \; | sort --ignore-case)
                if [[ -z "${a[*]}" ]] ; then
                    printf '%s\n' 'ERROR: No directories were found in /directories.' && sleep 2 && continue 2
                fi

                printf '%s\n' 'Select directory...'
                select sDirectory in "${a[@]}" ; do
                    break
                done
                # need to create array of directory categories here so it's available if user rebuilds directory without doing anything else first
                mapfile -t aCatFiles < <(find "directories/${sDirectory}/cats/" -mindepth 1 -maxdepth 1 -type 'f' -exec basename {} \;)
            ;;

            ('Add a category')
                sItem=''
                read -erp 'Enter category name: '
                [[ -z "${REPLY}" ]] && continue 2
                if [[ -f "directories/${sDirectory}/cats/${REPLY}" ]] ; then
                    printf '%s\n' 'ERROR: The specified category already exists.' && sleep 2 && continue 2
                fi
                touch -- "directories/${sDirectory}/cats/${REPLY}"
                sCat="${REPLY}"
                aCatFiles+=("${sCat}")
                continue 2
            ;;

            ('Select category')
                # build array of categories from the files in the /cats folder
                select sCat in "${aCatFiles[@]}" ; do
                    break
                done
                [[ -z "${aCatFiles[*]}" ]] && printf '%s\n' 'ERROR: A category must be created first.' && sleep 2
                continue 2
            ;;

            ('Add primary website'|'Add secondary website')
                if [[ "${sSelect}" = 'Add primary website' && -n "${sItem}" ]] ; then
                    a=(
                        'WARNING: There is an unpublished item in the queue.'
                        'Continue anyway and discard the queued item? [y/N]'
                    )
                    printf '%s\n' "${a[@]}"
                    read -rsN1
                    [[ "${REPLY}" != 'y' ]] && sItem='' && continue 2
                fi

                read -erp 'Website title: '
                [[ -z "${REPLY}" ]] && sItem='' && continue 2
                sTitle="${REPLY}"
                if [[ -f "directories/${sDirectory}/output/index.html" ]] ; then
                    if grep --fixed-strings --ignore-case ">${sTitle}<" -- "directories/${sDirectory}/output/index.html" > '/dev/null' ; then
                        a=(
                            "NOTICE: '${sTitle}' is already in the directory."
                            'Do you want to continue anyway? [y/N]'
                        )
                        printf '%s\n' "${a[@]}"
                        read -rsN1
                        [[ "${REPLY}" != 'y' ]] && sItem='' && continue 2
                    fi
                fi

                # strip/encode unsafe chars for title
                sTitle="$(perl -fpe 's|[^[:print:]]||g' <<< "${sTitle}")"
                sTitle="$(iconv --to-code='ASCII//TRANSLIT' <<< "${sTitle}")"
                sTitle="$(sed -E 's/[[:space:]]{2,}/ /g' <<< "${sTitle}")"
                for sKey in "${!aaPrintChars[@]}" ; do
                    sTitle="${sTitle//"${sKey}"/"${aaPrintChars[${sKey}]}"}"
                done

                if [[ "${sSelect}" = 'Add primary website' ]] ; then
                    # website description
                    read -erp 'One-line website description: '
                    [[ -z "${REPLY}" ]] && sItem='' && continue 2
                    sDescrip="${REPLY}"
                    if [[ -n "${sDescrip}" ]] ; then
                        # strip/encode unsafe chars for description
                        sDescrip="$(perl -fpe 's|[^[:print:]]||g' <<< "${sDescrip}")"
                        sDescrip="$(iconv --to-code='ASCII//TRANSLIT' <<< "${sDescrip}")"
                        sDescrip="$(sed -E 's/[[:space:]]{2,}/ /g' <<< "${sDescrip}")"
                        for sKey in "${!aaPrintChars[@]}" ; do
                            sDescrip="${sDescrip//"${sKey}"/"${aaPrintChars[${sKey}]}"}"
                        done
                    else
                        sDescrip='No description available.'
                    fi
                    sItem="<div class=\"gridItem\" tabindex=\"0\"><h3>${sTitle}</h3><p>${sDescrip}</p><ul>"
                    aPreview=(
                        "PRIMARY TITLE ..: ${sTitle}"
                        "DESCRIPTION ....: ${sDescrip}"
                    )
                else
                    aPreview+=("SECONDARY TITLE: ${sTitle}")
                fi

                # website url
                sERE='^https?:\/\/[a-z0-9-]+\.[a-z0-9-]+[a-z0-9.]*\/'
                while true ; do
                    read -erp 'Website URL: '
                    [[ -z "${REPLY}" ]] && sItem='' && continue 3
                    sUrl="${REPLY}"
                    [[ "${sUrl}" =~ ${sERE} ]] && break
                    a=(
                        ''
                        'WARNING: The URL appears to be invalid.'
                        'Do you want to accept it anyway? [y/N]'
                    )
                    printf '%s\n' "${a[@]}"
                    read -rsN1
                    [[ "${REPLY}" = 'y' ]] && break
                done

                # strip/encode unsafe chars in url
                sCleanUrl="$(perl -fpe 's|[^[:print:]]||g' <<< "${sUrl}")"
                sCleanUrl="$(iconv -c --to-code='ASCII//TRANSLIT' <<< "${sCleanUrl}")"
                for sKey in "${!aaUrlChars[@]}" ; do
                    sCleanUrl="${sCleanUrl//"${sKey}"/"${aaUrlChars[${sKey}]}"}"
                done
                if [[ "${sCleanUrl}" != "${sUrl}" ]] ; then
                    xdg-open "${sCleanUrl}" &> '/dev/null' & disown
                    a=(
                        ''
                        "Original URL : ${sUrl}"
                        "Cleaned URL .: ${sCleanUrl}"
                        'The cleaned URL was opened in your browser.'
                        'Is the URL correct? [Y/n]'
                    )
                    printf '%s\n' "${a[@]}"
                    read -rsN1
                    [[ "${REPLY}" = 'n' ]] && aPreview=() sItem='' && continue 2
                fi
                sDisplayUrl="${sCleanUrl%/}" # strip trailing slash if there is one
                sItem+="<li><a href=\"${sCleanUrl}\">${sDisplayUrl}</a></li>"
                aPreview+=("URL ............: ${sCleanUrl}")
            ;;

            ('Rebuild directory'|'Publish item')
                [[ -z "${sDirectory}" ]] && printf '%s\n' 'ERROR: No directory selected.' && sleep 2 && continue 2

                if [[ "${sSelect}" = 'Rebuild directory' ]] ; then
                    printf '%s\n' 'Rebuild directory? [Y/n]'
                    read -rsN1
                    [[ "${REPLY}" = 'n' ]] && sItem='' && continue 2
                fi

                if [[ "${sSelect}" = 'Publish item' ]] ; then
                    [[ -z "${sItem}" ]] && printf '%s\n' 'ERROR: There is nothing to publish.' && sleep 2 && continue 2

                    a=(
                        ''
                        'Item preview:'
                        '-------------'
                        "${aPreview[@]}"
                        '-----------------------------'
                        'Accept this submission? [Y/n]'
                    )
                    printf '%s\n' "${a[@]}"
                    read -rsN1
                    [[ "${REPLY}" = 'n' ]] && aPreview=() sItem='' && continue 2

                    printf '%s\n' 'Creating item file...'
                    printf '%s\n' "${sItem}</ul></div>" >> "directories/${sDirectory}/cats/${sCat}"
                    sort --ignore-case --output="directories/${sDirectory}/cats/${sCat}" -- "directories/${sDirectory}/cats/${sCat}"
                else
                    printf '%s\n' 'Backing up directory files...'
                    sGMT="$(date '+%F %H:%M:%S')"
                    mkdir --parents -- "backups/${sDirectory}/${sGMT}" || exit 1
                    cp --archive -- "directories/${sDirectory}/." "backups/${sDirectory}/${sGMT}/" || exit 1

                    printf '%s\n' 'Rebuilding category files...'
                    for sFile in "${aCatFiles[@]}" ; do
                        sort --ignore-case --output="directories/${sDirectory}/cats/${sFile}" -- "directories/${sDirectory}/cats/${sFile}"
                    done
                fi

                # delete old tidy log files
                for sFile in 'tidy-about.html.log' 'tidy-downloads.html.log' 'tidy-index.html.log' 'tidy-submissions.html.log' ; do
                    if [[ -f "${sFile}" ]] ; then
                        rm --preserve-root -- "${sFile}" || exit 1
                    fi
                done

                sDate="$(date --utc '+%b %d, %Y (GMT)')"
                for sFile in 'style.css' 'about.html' 'downloads.html' 'index.html' 'submissions.html' ; do
                    [[ ! -f "directories/${sDirectory}/templates/${sFile}" ]] && continue

                    printf '%s\n' "Building ${sFile}..."

                    if [[ "${sFile}" = 'style.css' ]] ; then
                        cp --force -- "directories/${sDirectory}/templates/${sFile}" "directories/${sDirectory}/output/" || exit 1
                        continue
                    fi

                    aFile=("$(<"directories/${sDirectory}/templates/head.html")")
                    aFile+=("$(<"directories/${sDirectory}/templates/${sFile}")")

                    if [[ "${sFile}" = 'index.html' ]] ; then
                        aFile=("${aFile[*]//'{PG_TITLE}'/"${sDirectory}"}")
                        # build category navigation menu and create all items
                        mapfile -t aCatFiles < <(printf '%s\n' "${aCatFiles[@]}" | sort --ignore-case)
                        for s in "${aCatFiles[@]}" ; do
                            # build cat nav and format anchor links in grid
                            s1="${s//' '/}"
                            s1="${s1//'&'/}"
                            s1="${s1^^}"
                            s2="${s//'&'/'&amp;'}" # encode '&' chars for link text
                            aFile+=(
                                "<h2 class=\"cat\" id=\"${s1}\">${s2}</h2>"
                                '<div class="grid">'
                                "$(<"directories/${sDirectory}/cats/${s}")"
                                '</div>'
                            )
                            sNav+="<a href=\"#${s1}\">${s2}</a> "
                        done
                        aFile=("${aFile[*]//'{CATNAV}'/"${sNav}"}")
                        aFile+=('</div>')
                    elif [[ "${sFile}" = 'about.html' ]] ; then
                        aFile=("${aFile[*]//'{PG_TITLE}'/"About | ${sDirectory}"}")
                    elif [[ "${sFile}" = 'downloads.html' ]] ; then
                        aFile=("${aFile[*]//'{PG_TITLE}'/"Downloads | ${sDirectory}"}")
                    elif [[ "${sFile}" = 'submissions.html' ]] ; then
                        aFile=("${aFile[*]//'{PG_TITLE}'/"Submissions | ${sDirectory}"}")
                    fi
                    aFile+=("$(<"directories/${sDirectory}/templates/tail.html")") # keep above variable replacements
                    aFile=("${aFile[*]//'{PG_TITLE}'/"${sDirectory}"}")
                    aFile=("${aFile[*]//'{TITLE}'/"${sDirectory}"}")
                    aFile=("${aFile[*]//'{DATE}'/"${sDate}"}")
                    aFile=("${aFile[*]//'{REPO_URL}'/"${sScriptRepoURL}"}")

                    printf '%s\n' 'Running tidy...'
                    # https://api.html-tidy.org/
                    if ! aFile=("$(tidy -config 'tidy.conf' <<< "${aFile[*]}" 2> "tidy-${sFile}.log")") ; then
                        a=(
                            "WARINING: tidy found problems with ${sFile}."
                            "See the tidy-${sFile}.log file."
                            'Press any key to continue.'
                        )
                        printf '%s\n' "${a[@]}" && read -rsN1
                    fi

                    printf '%s\n' "${aFile[*]}" > "directories/${sDirectory}/output/${sFile}" || exit 1
                done
                sItem='' sCat='' sNav=''

                xdg-open "directories/${sDirectory}/output/index.html" &> '/dev/null' & disown
                a=(
                    ''
                    'Finished!'
                    'The directory was opened in your web browser.'
                )
                if [[ "${sSelect}" = 'Rebuild directory' ]] ; then
                    a+=("Directory files were backed up to backups/${sDirectory}/${sGMT}")
                    printf '%s\n' "${a[@]}" && exit
                fi
                printf '%s\n' "${a[@]}" && sleep 2 && continue 2
            ;;

            ('Quit')
                if [[ -n "${sItem}" ]] ; then
                    a=(
                        ''
                        'WARNING: There is an unpublished item in the queue.'
                        'Exit and discard the queued item? [y/N]'
                    )
                    printf '%s\n' "${a[@]}"
                    read -rsN1
                    [[ "${REPLY}" != 'y' ]] && continue 2
                fi
                exit
            ;;

            (*) printf '%s\n' 'ERROR: Invalid choice.' && continue 2 ;;
        esac
        break
    done
done

exit
