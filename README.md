# Web Directory Builder

<img src="screencap.png" alt="screen-shot">

## INTRO

Web Directory Builder (wdb.sh) is a Bash shell script for building HTML directories of websites. The script outputs an index.html file containing a categorized list of all the resources you add, as well as several other files, all of which can be uploaded to a web server. You can see 2 sample pages here:

* [altTech](https://12bytes.org/alttech/)
* [Alternative Information Directory](https://12bytes.org/aid/)

The complete output consists of the following objects:

* index.html (main directory index)
* about.html (directory information)
* downloads.html (misc. downloads, banner images, etc.)
* submissions.html (instructions for submitting a resource or correction)
* style.css (style sheet)
* /resources (folder containing extra directory resources if any, such as images)

Each item in the directory index contains the following information:

1. website title
2. website description
3. optionally, one or more secondary websites, such as a video or social media channel
4. a link to the website and, optionally, links to the secondary website(s)

## CONFIGURE

Prior to creating your first web directory you should review the files in the `master-templates` folder. These files will provide the default templates used for all of the web directories. Note that changes to these files will not affect any existing web directories.

When a new item is published to a directory, or a directory is rebuilt, HTML Tidy is used to check the HTML. You may want to review the Tidy configuration file `tidy.conf`.

## USAGE

To run the script, `cd` to the script directory and run `$ ./wdb.sh`.

The first thing you'll need to do is create a new web directory. The name chosen for the directory will be used as the folder name for the directory files. Using 'My Directory' as an example, the files from the `master-templates` folder will be copied to `directories/My Directory/templates`. These files should then be edited to suit the needs of the directory. Any files you do not wish to use can be deleted.

The following variables may be used in any of the template files with the exception of style.css:

* `{PG_TITLE}` : Replaced with the page title and directory title (page title | directory title).
* `{TITLE}` : Replaced with the directory title.
* `{CATNAV}` : Replaced with a category navigation menu. Usable only in the index.html file.'
* `{DATE}` : 'Replaced with the UTC date.'
* `{REPO_URL}` : Replaced with the URL to the Web Directory Builder source code repository at Codeberg.

Upon creating a directory, the following objects will be created in the `directories/<directory name>/output` folder:

```
/resources
about.html
downloads.html
index.html
submissions.html
```

The `directories/<directory name>/assets` folder is a complimentary folder for storing objects which are associated with the directory but not used in the HTML files, such as uncompressed logo images for example. This folder is ignored by the script.

Any banner images, logos or other resources you wish to use in the directory pages should be placed in `directories/<directory name>/output/resources`.

Following is a suggested template when adding a secondary website:

`<title/channel> (<platform>)`

For example: Mr. Kitten (YouTube)

If you need to edit or remove an item from the directory, edit the associated text file in the `directories/<directory name>/cats` folder, run the script, then select the directory and choose the 'Rebuild directory' menu item.

If you need to edit any of the HTML files for the directory other than the directory items, or you need to edit the style.css file, edit the files in the `/directories/<directory name>/templates` folder and rebuild the directory.

When rebuilding or adding items to the directory, the current directory files are first backed up to `backups/<directory name>/<date-time>`.
